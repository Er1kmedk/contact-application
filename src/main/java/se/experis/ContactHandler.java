package se.experis;

import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;

public class ContactHandler {

    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_GREEN  = "\u001B[32m";

    ArrayList<Contact> contactList;

    public ContactHandler() {
        contactList = new ArrayList<Contact>();

        Contact contactOne = new Contact("Erik Johansson", "920630", "Tveten 1", "0768359303");
        Contact contactTwo = new Contact("Frank Larsson", "980530", "Stallvägen 11", "0768359304");
        Contact contactThree = new Contact("Timme Biörchk", "850613", "Torslanda 3", generatePhoneNumber());
        Contact contactFour = new Contact("Lo Andersson", "750808", "Trollhättan 9 ", generatePhoneNumber());
        Contact contactFive = new Contact("Bo Andersson", "200613", "Rottne 3", generatePhoneNumber());
        Contact contactSix = new Contact("Harry Potter", "950718", "Privet drive", generatePhoneNumber());
        Contact contactSeven = new Contact("Yoda", "101010", "Kasheek", generatePhoneNumber());
        Contact contactEight = new Contact("Darth Vader", "666", "Death star", generatePhoneNumber());
        Contact contactNine = new Contact("Ragnar Löf", "860613", "Växjö 101", generatePhoneNumber());
        Contact contactTen = new Contact("Ahmed Walla", "960603", "Göteborg 34", generatePhoneNumber());

        contactList.add(contactOne);
        contactList.add(contactTwo);
        contactList.add(contactThree);
        contactList.add(contactFour);
        contactList.add(contactFive);
        contactList.add(contactSix);
        contactList.add(contactSeven);
        contactList.add(contactEight);
        contactList.add(contactNine);
        contactList.add(contactTen);

    }

    public String searchFor(String searchWord) {
        String searchResult =  ANSI_GREEN + "\"" + searchWord + "\" " + ANSI_RESET + "was found in, ";
        boolean foundSearchWord = false;

        for (Contact contact : contactList) {

            if (contact.getFullName().contains(searchWord)) {
                searchResult += contact.getFullName().replace(searchWord, ANSI_GREEN + searchWord + ANSI_RESET ) + ", ";
                foundSearchWord = true;
            }
            if (contact.getDateOfBirth().contains(searchWord)) {
                searchResult += contact.getDateOfBirth().replace(searchWord, ANSI_GREEN + searchWord + ANSI_RESET ) + ", ";
                foundSearchWord = true;
            }
            if (contact.getAddress().contains(searchWord)) {
                searchResult += contact.getAddress().replace(searchWord,  ANSI_GREEN + searchWord + ANSI_RESET ) + ", ";
                foundSearchWord = true;
            }
            if (contact.getMobileNumber().contains(searchWord)) {
                searchResult += contact.getMobileNumber().replace(searchWord, ANSI_GREEN + searchWord + ANSI_RESET ) + ", ";
                foundSearchWord = true;
            }
            if (!foundSearchWord) {
                searchResult = ANSI_GREEN + "\"" + searchWord + "\" " + ANSI_RESET + "...Could not b found in any contact :(";
            }
        }
        return searchResult;
    }

    // Rather than writing all the fake telephone numbers i simply generate a random representation
    public String generatePhoneNumber() {
        String randomBirthDate = "";

        for (int i = 0; i < 10; i++) {
            randomBirthDate += ThreadLocalRandom.current().nextInt(0, 10);

        }

        return randomBirthDate;
    }
}
