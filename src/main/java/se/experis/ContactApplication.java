package se.experis;

import java.util.Scanner;

public class ContactApplication {
    public static void main(String[] args) {
        Scanner inputScanner =  new Scanner(System.in);
        ContactHandler contactHandler = new ContactHandler();
        System.out.println("Welcome to the contact search application! \nAvailable search options are Name, Address, Mobile Number or Date of birth.\nNotice that the search is Case sensitive!\n");

        System.out.println("Please enter search word: ");

        String searchWord = inputScanner.nextLine();

        String searchResult = contactHandler.searchFor(searchWord);
        System.out.println("Search result: " + searchResult);
    }
}
