package se.experis;

public class Contact {

    protected String fullName;
    protected String dateOfBirth;
    protected String address;
    protected String mobileNumber;
    protected String workNumber;


    public Contact(String fullName, String dateOfBirth, String address, String mobileNumber) {
        this.fullName = fullName;
        this.dateOfBirth = dateOfBirth;
        this.address = address;
        this.mobileNumber = mobileNumber;
    }


    public String getFullName() {
        return fullName;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public String getAddress() {
        return address;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public String getWorkNumber() {
        return workNumber;
    }
}
